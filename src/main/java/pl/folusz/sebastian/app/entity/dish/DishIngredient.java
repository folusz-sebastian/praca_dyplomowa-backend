package pl.folusz.sebastian.app.entity.dish;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.io.Serializable;

@NoArgsConstructor
@Getter @Setter
@Entity
@Table(name = "dish_ingredient")
public class DishIngredient implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dish_ingredient_id")
    private long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "dish_id")
    private Dish dish;

    @ManyToOne(cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name = "ingredient_id")
    private Ingredient ingredient;

    @Digits(integer = 5, fraction = 2)
    @Column(name = "quantity")
    private double quantity;

    public DishIngredient(Dish dish, Ingredient ingredient, @Digits(integer = 5, fraction = 2) double quantity) {
        this.dish = dish;
        this.ingredient = ingredient;
        this.quantity = quantity;
    }
}
