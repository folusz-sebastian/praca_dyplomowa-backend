package pl.folusz.sebastian.app.entity.dish;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter @Setter
@Entity
@Table(name = "measurement_unit")
public class MeasurementUnit {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "measurement_unit_id")
    private long id;

    @Column(name = "name")
    private String name;
}
