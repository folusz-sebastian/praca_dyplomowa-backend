package pl.folusz.sebastian.app.entity.reservation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import pl.folusz.sebastian.app.entity.restaurantTable.RestaurantTable;
import pl.folusz.sebastian.app.entity.user.User;

import javax.persistence.*;
import java.util.Date;

@AllArgsConstructor
@Getter @Setter
@Entity
@Table(name = "reservation")
public class Reservation {

    public Reservation() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reservation_id")
    private long id;

    @Temporal(TemporalType.DATE)
    @Column(name = "date")
    private Date date;

    @Column(name = "beginning_hour")
    @JsonProperty("beginning_hour")
    private int beginningHour;

    @Column(name = "duration")
    private int duration;

    @ManyToOne(cascade =
            {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}
            )
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "restaurant_table_id")
    private RestaurantTable table;
}
