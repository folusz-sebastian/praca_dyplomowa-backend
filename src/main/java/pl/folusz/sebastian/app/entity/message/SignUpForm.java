package pl.folusz.sebastian.app.entity.message;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SignUpForm {
    private String name;

    @JsonProperty("last_name")
    private String lastName;

    private String email;
    private String password;
}
