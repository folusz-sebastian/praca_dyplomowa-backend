package pl.folusz.sebastian.app.entity.dish;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "dish")
public class Dish {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dish_id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    @Digits(integer = 5, fraction = 2)
    private double price;

    @Column(name = "description")
    private String description;

    @Column(name = "image_url")
    @JsonProperty("image_url")
    private String imageUrl;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "category_id")
    private DishCategory category;

    @OneToMany(mappedBy = "dish", cascade = CascadeType.ALL)
    @JsonProperty("dish_ingredients")
    private List<DishIngredient> dishIngredients;
}
