package pl.folusz.sebastian.app.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TablePlaceDto {
    private long id;
    private String name;
}
