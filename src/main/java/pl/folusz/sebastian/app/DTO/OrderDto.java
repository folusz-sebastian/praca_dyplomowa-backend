package pl.folusz.sebastian.app.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import pl.folusz.sebastian.app.entity.order.OrderStatus;

import java.util.Date;
import java.util.List;

@Getter @Setter
public class OrderDto {
    private long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Europe/Warsaw")
    private Date dateTime;

    private OrderStatus status;
    private double cost;

    @JsonProperty("dish_orders")
    private List<DishOrderDto> dishOrders;

    private ReservationDto reservation;
}
