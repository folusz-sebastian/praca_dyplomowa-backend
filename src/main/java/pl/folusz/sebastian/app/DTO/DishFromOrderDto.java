package pl.folusz.sebastian.app.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import pl.folusz.sebastian.app.entity.dish.DishCategory;

import javax.validation.constraints.Digits;

@Getter
@Setter
public class DishFromOrderDto {
    private long id;
    private String name;

    @Digits(integer = 5, fraction = 2)
    private double price;

    @JsonProperty("image_url")
    private String imageUrl;

    private String description;
    private DishCategory category;
}
