package pl.folusz.sebastian.app.DTO;

import lombok.Getter;
import lombok.Setter;
import pl.folusz.sebastian.app.entity.restaurantTable.TablePlace;

@Getter @Setter
public class RestaurantTableDto {
    long id;
    int chairs;
    TablePlace place;
}
