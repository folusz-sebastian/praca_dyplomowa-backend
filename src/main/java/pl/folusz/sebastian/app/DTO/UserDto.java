package pl.folusz.sebastian.app.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import pl.folusz.sebastian.app.entity.user.UserRole;

import java.util.List;

@Getter @Setter
public class UserDto {
    private long id;
    private String email;
    private String name;

    @JsonProperty("last_name")
    private String lastName;

    private int active;
    private List<UserRole> roles;
}
