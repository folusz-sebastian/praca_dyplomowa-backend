package pl.folusz.sebastian.app.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import pl.folusz.sebastian.app.entity.dish.MeasurementUnit;

@Getter @Setter
public class IngredientDto {
    private long id;
    private String name;

    @JsonProperty("measurement_unit")
    private MeasurementUnit measurementUnit;
}
