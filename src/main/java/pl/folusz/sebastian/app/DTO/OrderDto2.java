package pl.folusz.sebastian.app.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import pl.folusz.sebastian.app.entity.order.OrderStatus;

import java.util.Date;

@Getter @Setter
public class OrderDto2 {
    private long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Europe/Warsaw")
    private Date dateTime;

    private OrderStatus status;
    private double cost;
}
