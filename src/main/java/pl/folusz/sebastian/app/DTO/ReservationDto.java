package pl.folusz.sebastian.app.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter @Setter
public class ReservationDto {
    long id;
    @JsonFormat(pattern="yyyy-MM-dd")
    Date date;
    @JsonProperty("beginning_hour")
    int beginningHour;
    int duration;
    RestaurantTableDto table;
}
