package pl.folusz.sebastian.app.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import pl.folusz.sebastian.app.DTO.DishDto;
import pl.folusz.sebastian.app.DTO.DishIngredientDto;
import pl.folusz.sebastian.app.DTO.IngredientDto;
import pl.folusz.sebastian.app.Mapper.DishIngredientMapper;
import pl.folusz.sebastian.app.Mapper.DishMapper;
import pl.folusz.sebastian.app.Mapper.IngredientMapper;
import pl.folusz.sebastian.app.dao.dish.*;
import pl.folusz.sebastian.app.entity.dish.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DishService {
    private DishRepository dishRepository;
    private IngredientRepository ingredientRepository;
    private DishIngredientRepository dishIngredientRepository;
    private DishCategoryRepository dishCategoryRepository;
    private MeasurementUnitRepository measurementUnitRepository;

    public DishService(DishRepository dishRepository, IngredientRepository ingredientRepository,
                       DishIngredientRepository dishIngredientRepository, DishCategoryRepository dishCategoryRepository,
                       MeasurementUnitRepository measurementUnitRepository) {
        this.dishRepository = dishRepository;
        this.ingredientRepository = ingredientRepository;
        this.dishIngredientRepository = dishIngredientRepository;
        this.dishCategoryRepository = dishCategoryRepository;
        this.measurementUnitRepository = measurementUnitRepository;
    }

    public DishDto save(Dish dish) {
        List<DishIngredient> oldDishIngredientList = dish.getDishIngredients();
        dish.setDishIngredients(null);

        DishCategory dishCategory = saveDishCategory(dish);
        dish.setCategory(dishCategory);

        dish = dishRepository.save(dish);
        DishDto dishDto = DishMapper.MAPPER.dishToDishDto(dish);
        dishDto.setCategory(dishCategory);

        if (oldDishIngredientList != null) {
            List<DishIngredientDto> dishIngredientListDto = new ArrayList<>();

            for (DishIngredient dishIngredient : oldDishIngredientList) {
                dishIngredient.getIngredient().setMeasurementUnit(
                        saveMeasurementUnit(dishIngredient)
                );

                dishIngredient.setIngredient(saveIngredient(dishIngredient));

                IngredientDto ingredientDto =
                        IngredientMapper.MAPPER.ingredientToIngrdientDto(dishIngredient.getIngredient());
                DishIngredient newDishIngredient = new DishIngredient(
                        dish,
                        dishIngredient.getIngredient(),
                        dishIngredient.getQuantity()
                );

                DishIngredientDto newDishIngredientDto = DishIngredientMapper.MAPPER.dishIngredientToDishIngredientDto(
                        dishIngredientRepository.save(newDishIngredient)
                );
                newDishIngredientDto.setIngredient(ingredientDto);
                dishIngredientListDto.add(
                        newDishIngredientDto
                );
            }
            dishDto.setDishIngredients(dishIngredientListDto);
        }

        return dishDto;
    }

    private Ingredient saveIngredient(DishIngredient dishIngredient) {
        Ingredient ingredient;
        if (ingredientRepository.existsByNameAndMeasurementUnit(
                dishIngredient.getIngredient().getName(),
                dishIngredient.getIngredient().getMeasurementUnit()
        )) {
            ingredient = ingredientRepository.save(
                    ingredientRepository.findByNameAndMeasurementUnit(
                            dishIngredient.getIngredient().getName(),
                            dishIngredient.getIngredient().getMeasurementUnit()
                    )
            );
        } else {
            ingredient = ingredientRepository.save(dishIngredient.getIngredient());
        }
        return ingredient;
    }

    private DishCategory saveDishCategory(Dish dish) {
        DishCategory dishCategory;
        if (dishCategoryRepository.existsByName(dish.getCategory().getName())) {
            dishCategory = dishCategoryRepository.findByName(dish.getCategory().getName());
        } else {
            dishCategory = dishCategoryRepository.save(dish.getCategory());
        }
        return dishCategory;
    }

    private MeasurementUnit saveMeasurementUnit(DishIngredient dishIngredient) {
        MeasurementUnit measurementUnit;
        if (measurementUnitRepository
                .existsByName(dishIngredient.getIngredient().getMeasurementUnit().getName())) {
            measurementUnit = measurementUnitRepository.findByName(
                    dishIngredient.getIngredient().getMeasurementUnit().getName()
            );
        } else {
            measurementUnit = measurementUnitRepository.save(
                    dishIngredient.getIngredient().getMeasurementUnit()
            );
        }
        return measurementUnit;
    }

    public List<DishDto> findAll() {
        return DishMapper.MAPPER.dishToDishDto(dishRepository.findAll());
    }

    public List<DishDto> findAllLimitedBy(int elements) {
        return DishMapper.MAPPER.dishToDishDto(
                dishRepository.findAll(PageRequest.of(0, elements)).getContent()
        );
    }

    public DishDto findById(long id) {
        Optional<Dish> dish = dishRepository.findById(id);
        return dish.map(DishMapper.MAPPER::dishToDishDto).orElse(null);
    }

    public List<DishDto> findAllFromCategory(String dishCategory) {
        DishCategory category = dishCategoryRepository.findByName(dishCategory);
        return DishMapper.MAPPER.dishToDishDto(
                dishRepository.findByCategory(category)
        );
    }
}
