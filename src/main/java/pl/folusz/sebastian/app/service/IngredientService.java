package pl.folusz.sebastian.app.service;

import org.springframework.stereotype.Service;
import pl.folusz.sebastian.app.DTO.IngredientDto;
import pl.folusz.sebastian.app.Mapper.IngredientMapper;
import pl.folusz.sebastian.app.dao.dish.IngredientRepository;
import pl.folusz.sebastian.app.entity.dish.Ingredient;

import java.util.List;

@Service
public class IngredientService {
    private IngredientRepository ingredientRepository;

    public IngredientService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    public Ingredient save(Ingredient ingredient) {
        return this.ingredientRepository.save(ingredient);
    }

    public List<IngredientDto> findAll() {
        return IngredientMapper.MAPPER.ingredientToIngrdientDto(
                this.ingredientRepository.findAll()
        );
    }
}
