package pl.folusz.sebastian.app.service;

import org.springframework.stereotype.Service;
import pl.folusz.sebastian.app.DTO.RestaurantTableDto;
import pl.folusz.sebastian.app.Mapper.RestaurantTableMapper;
import pl.folusz.sebastian.app.dao.restaurantTable.RestaurantTableRepository;
import pl.folusz.sebastian.app.dao.restaurantTable.TablePlaceRepository;
import pl.folusz.sebastian.app.entity.restaurantTable.RestaurantTable;
import pl.folusz.sebastian.app.entity.restaurantTable.TablePlace;

import java.util.List;
import java.util.Optional;

@Service
public class RestaurantTableService {
    private RestaurantTableRepository restaurantTableRepository;
    private TablePlaceRepository tablePlaceRepository;

    public RestaurantTableService(RestaurantTableRepository restaurantTableRepository, TablePlaceRepository tablePlaceRepository) {
        this.restaurantTableRepository = restaurantTableRepository;
        this.tablePlaceRepository = tablePlaceRepository;
    }

    public RestaurantTable save(RestaurantTable restaurantTable) {
        TablePlace tablePlace = tablePlaceRepository.findByName(restaurantTable.getPlace().getName());
        if (tablePlace == null) {
            tablePlace = tablePlaceRepository.save(new TablePlace(restaurantTable.getPlace().getName()));
        }
        restaurantTable.setPlace(tablePlace);
        return restaurantTableRepository.save(restaurantTable);
    }

    public List<RestaurantTableDto> findAll() {
        return RestaurantTableMapper.MAPPER.restaurantTableToRestaurantTableDto(restaurantTableRepository.findAll());
    }

    public void delete(long id) {
        Optional<RestaurantTable> restaurantTableOptional = restaurantTableRepository.findById(id);
        restaurantTableOptional.ifPresent(restaurantTable -> restaurantTableRepository.delete(restaurantTable));
    }

    public List<RestaurantTableDto> findAllFree(String year, String month, String day, int beginningHour, int duration) {
        return RestaurantTableMapper.MAPPER.restaurantTableToRestaurantTableDto(
                this.restaurantTableRepository.findAllFree(year, month, day, beginningHour, duration)
        );
    }
}
