package pl.folusz.sebastian.app.service;

import org.springframework.stereotype.Service;
import pl.folusz.sebastian.app.dao.dish.DishCategoryRepository;
import pl.folusz.sebastian.app.entity.dish.DishCategory;

import java.util.List;

@Service
public class DishCategoryService {
    private DishCategoryRepository repository;

    public DishCategoryService(DishCategoryRepository repository) {
        this.repository = repository;
    }

    public List<DishCategory> findAll() {
        return this.repository.findAll();
    }
}
