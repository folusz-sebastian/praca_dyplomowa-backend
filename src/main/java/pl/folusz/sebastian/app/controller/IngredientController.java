package pl.folusz.sebastian.app.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.folusz.sebastian.app.DTO.IngredientDto;
import pl.folusz.sebastian.app.entity.dish.Ingredient;
import pl.folusz.sebastian.app.service.IngredientService;

import java.util.List;

@RestController
@RequestMapping("api")
public class IngredientController {
    private IngredientService ingredientService;

    public IngredientController(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }

    @PostMapping("/ingredients")
    @ResponseBody
    public ResponseEntity<Ingredient> save(@RequestBody Ingredient ingredient) {
        return ResponseEntity.ok(ingredientService.save(ingredient));
    }

    @GetMapping("/ingredients")
    @ResponseBody
    public ResponseEntity<List<IngredientDto>> findAll() {
        return ResponseEntity.ok(this.ingredientService.findAll());
    }
}
