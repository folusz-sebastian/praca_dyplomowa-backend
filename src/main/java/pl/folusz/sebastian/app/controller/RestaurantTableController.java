package pl.folusz.sebastian.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.folusz.sebastian.app.DTO.RestaurantTableDto;
import pl.folusz.sebastian.app.entity.restaurantTable.RestaurantTable;
import pl.folusz.sebastian.app.service.RestaurantTableService;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RestaurantTableController {
    private RestaurantTableService restaurantTableService;

    @Autowired
    public RestaurantTableController(RestaurantTableService restaurantTableService) {
        this.restaurantTableService = restaurantTableService;
    }

    @PostMapping("/tables")
    @ResponseBody
    public ResponseEntity<RestaurantTable> save(@RequestBody RestaurantTable restaurantTable) {
        return ResponseEntity.ok(restaurantTableService.save(restaurantTable));
    }

    @GetMapping("/tables")
    @ResponseBody
    public ResponseEntity<List<RestaurantTableDto>> findAll() {
        return ResponseEntity.ok(restaurantTableService.findAll());
    }

    @DeleteMapping("/tables/{id}")
    public void delete(@PathVariable long id) {
        restaurantTableService.delete(id);
    }

    @GetMapping("/free_tables")
    public ResponseEntity<List<RestaurantTableDto>> findAllFree(
            @RequestParam(name = "year") String year,
            @RequestParam(name = "month") String month,
            @RequestParam(name = "day") String day,
            @RequestParam(name = "beginning_hour") int beginningHour,
            @RequestParam(name = "duration") int duration
    ) {
        return ResponseEntity.ok(restaurantTableService.findAllFree(year, month, day, beginningHour, duration));
    }
}
