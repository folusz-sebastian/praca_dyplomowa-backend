package pl.folusz.sebastian.app.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.folusz.sebastian.app.entity.dish.DishCategory;
import pl.folusz.sebastian.app.service.DishCategoryService;

import java.util.List;

@RestController
@RequestMapping("api")
public class DishCategoryController {
    private DishCategoryService dishCategoryService;

    public DishCategoryController(DishCategoryService dishCategoryService) {
        this.dishCategoryService = dishCategoryService;
    }

    @GetMapping("/dishCategories")
    @ResponseBody
    public ResponseEntity<List<DishCategory>> findAll() {
        return ResponseEntity.ok(dishCategoryService.findAll());
    }
}
