package pl.folusz.sebastian.app.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.folusz.sebastian.app.entity.dish.MeasurementUnit;
import pl.folusz.sebastian.app.service.MeasurementUnitService;

import java.util.List;

@RestController
@RequestMapping("api")
public class MeasurementUnitController {
    private MeasurementUnitService service;

    public MeasurementUnitController(MeasurementUnitService service) {
        this.service = service;
    }

    @GetMapping("/measurementUnits")
    @ResponseBody
    public ResponseEntity<List<MeasurementUnit>> findAll() {
        return ResponseEntity.ok(this.service.findAll());
    }
}
