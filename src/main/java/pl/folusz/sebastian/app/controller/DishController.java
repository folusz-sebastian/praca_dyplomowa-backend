package pl.folusz.sebastian.app.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.folusz.sebastian.app.DTO.DishDto;
import pl.folusz.sebastian.app.entity.dish.Dish;
import pl.folusz.sebastian.app.service.DishService;

import java.util.List;

@RestController
@RequestMapping("api")
public class DishController {
    private DishService dishService;

    public DishController(DishService dishService) {
        this.dishService = dishService;
    }

    @PostMapping("/dishes")
    @ResponseBody
    public ResponseEntity<DishDto> save(@RequestBody Dish dish) {
        return ResponseEntity.ok(dishService.save(dish));
    }

    @GetMapping("/dishes")
    @ResponseBody
    public List<DishDto> findAll(@RequestParam(required = false) Integer elements,
                                 @RequestParam(required = false, name = "category") String dishCategory) {
        if (elements != null) {
            return dishService.findAllLimitedBy(elements);
        } else if (dishCategory != null) {
            return dishService.findAllFromCategory(dishCategory);
        }
        return dishService.findAll();
    }

    @GetMapping("/dishes/{dishId}")
    @ResponseBody
    public DishDto findAll(@PathVariable long dishId) {
        return dishService.findById(dishId);
    }
}
