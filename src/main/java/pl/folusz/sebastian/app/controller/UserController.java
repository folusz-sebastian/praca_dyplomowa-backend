package pl.folusz.sebastian.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import pl.folusz.sebastian.app.DTO.UserDto;
import pl.folusz.sebastian.app.entity.message.ResponseMessage;
import pl.folusz.sebastian.app.entity.message.ResponseMessageWithUser;
import pl.folusz.sebastian.app.entity.message.SignUpForm;
import pl.folusz.sebastian.app.entity.user.User;
import pl.folusz.sebastian.app.service.UserService;


@RestController
@RequestMapping("/api")
public class UserController {
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/user")
    @ResponseBody
    public ResponseEntity<UserDto> currentUser(Authentication authentication) {
        return new ResponseEntity<>(
                userService.findByEmail(authentication.getName()),
                HttpStatus.OK
        );
    }

    @PostMapping("/signup")
    @ResponseBody
    public ResponseEntity<?> createNewUser(@RequestBody SignUpForm signUpForm) {
        if (userService.existsByEmail(signUpForm.getEmail())) {
            return new ResponseEntity<>(new ResponseMessage("Użytkownik o podanym adresie e-mail już istnieje!"),
                    HttpStatus.BAD_REQUEST);
        }
        User user = new User();
        user.setName(signUpForm.getName());
        user.setLastName(signUpForm.getLastName());
        user.setPassword(signUpForm.getPassword());
        user.setEmail(signUpForm.getEmail());

        user = userService.save(user);
        return new ResponseEntity<>(
                new ResponseMessageWithUser("Użytkownik został zarejestrowany pomyślnie!", user),
                HttpStatus.OK
        );
    }
}
