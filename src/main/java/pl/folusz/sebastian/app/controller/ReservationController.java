package pl.folusz.sebastian.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import pl.folusz.sebastian.app.DTO.ReservationDto;
import pl.folusz.sebastian.app.entity.reservation.Reservation;
import pl.folusz.sebastian.app.service.ReservationService;

import java.util.List;

@RestController
@RequestMapping("api")
public class ReservationController {
    private ReservationService reservationService;

    @Autowired
    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @GetMapping("/reservations")
    @ResponseBody
    public ResponseEntity<List<ReservationDto>> findAllWithoutUser() {
        return ResponseEntity.ok(reservationService.findAllWithoutUser());
    }

    @GetMapping("my_reservations")
    @ResponseBody
    public ResponseEntity<List<ReservationDto>> findByUser(Authentication authentication) {
        return ResponseEntity.ok(reservationService.findByUser(authentication.getName()));
    }

    @PostMapping("/reservations")
    @ResponseBody
    public ResponseEntity<ReservationDto> save(@RequestBody Reservation reservation, Authentication authentication) {
        return ResponseEntity.ok(reservationService.save(reservation, authentication.getName()));
    }
}
