package pl.folusz.sebastian.app.dao.restaurantTable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.folusz.sebastian.app.entity.restaurantTable.RestaurantTable;

import java.util.List;

//@PreAuthorize("hasAuthority('ADMIN')")
public interface RestaurantTableRepository extends JpaRepository<RestaurantTable, Long> {
    @Query(
            value = "select * from restaurant_table\n" +
                    "where table_id not in\n" +
                    "      (\n" +
                    "        select r.restaurant_table_id\n" +
                    "        from restaurant_table r_table\n" +
                    "               left join reservation r on r_table.table_id = r.restaurant_table_id\n" +
                    "        where year(date) = :year\n" +
                    "          and month(date) = :month\n" +
                    "          and day(date) = :day\n" +
                    "          and (\n" +
                    "            (:beginningHour >= r.beginning_hour and :beginningHour < (r.beginning_hour + r.duration))\n" +
                    "            or\n" +
                    "            (:beginningHour < r.beginning_hour and (:beginningHour + :duration) > r.beginning_hour)\n" +
                    "          )\n" +
                    "        group by r.restaurant_table_id\n" +
                    "      );",
            nativeQuery = true
    )
    List<RestaurantTable> findAllFree(
            @Param("year") String year,
            @Param("month") String month,
            @Param("day") String day,
            @Param("beginningHour") int beginningHour,
            @Param("duration") int duration
    );
}