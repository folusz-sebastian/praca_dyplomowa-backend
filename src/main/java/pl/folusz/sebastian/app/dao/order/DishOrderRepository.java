package pl.folusz.sebastian.app.dao.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.folusz.sebastian.app.entity.order.DishOrder;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DishOrderRepository extends JpaRepository<DishOrder, Long> {
    @Query("from DishOrder dishOrder " +
            "where dishOrder.order.reservation.user.email = :userEmail " +
            "order by dishOrder.order.dateTime desc"
    )
    List<DishOrder> findByUser(@Param("userEmail") String userEmail);

    @Query("from DishOrder dishOrder " +
            "where dishOrder.order.reservation.user.email = :userEmail " +
            "order by dishOrder.order.dateTime desc"
    )
    List<DishOrder> findByUserAndLimitBy(@Param("userEmail") String userEmail, Pageable pageable);

    @Override
    @Query("from DishOrder dishOrder " +
            "order by dishOrder.order.dateTime desc"
    )
    List<DishOrder> findAll();
}
