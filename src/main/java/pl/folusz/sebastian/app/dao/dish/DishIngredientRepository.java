package pl.folusz.sebastian.app.dao.dish;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.folusz.sebastian.app.entity.dish.DishIngredient;

public interface DishIngredientRepository extends JpaRepository<DishIngredient, Long> {
}
