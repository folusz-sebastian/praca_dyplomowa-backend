package pl.folusz.sebastian.app.dao.reservation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.folusz.sebastian.app.entity.reservation.Reservation;

import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    @Query(value = "select * from reservation r inner join user u on r.user_id = u.user_id " +
            "where u.email = :email and date(r.date) >= date(current_date)",
            nativeQuery = true
    )
    List<Reservation> findByUserEmailFromToday(@Param("email") String userEmail);
}
