package pl.folusz.sebastian.app.dao.restaurantTable;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.folusz.sebastian.app.entity.restaurantTable.TablePlace;

public interface TablePlaceRepository extends JpaRepository<TablePlace, Long> {
    TablePlace findByName(String name);
}
