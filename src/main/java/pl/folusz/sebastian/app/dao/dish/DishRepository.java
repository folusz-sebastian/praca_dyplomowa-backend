package pl.folusz.sebastian.app.dao.dish;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.folusz.sebastian.app.entity.dish.Dish;
import pl.folusz.sebastian.app.entity.dish.DishCategory;

import java.util.List;

public interface DishRepository extends JpaRepository<Dish, Long> {
    List<Dish> findByCategory(DishCategory category);
}
