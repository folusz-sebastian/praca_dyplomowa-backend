package pl.folusz.sebastian.app.Mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.folusz.sebastian.app.DTO.DishIngredientDto;
import pl.folusz.sebastian.app.entity.dish.DishIngredient;

import java.util.List;

@Mapper
public interface DishIngredientMapper {
    DishIngredientMapper MAPPER = Mappers.getMapper(DishIngredientMapper.class);

    DishIngredientDto dishIngredientToDishIngredientDto(DishIngredient dishIngredient);
    List<DishIngredientDto> dishIngredientToDishIngredientDto(List<DishIngredient> dishIngredient);
}
