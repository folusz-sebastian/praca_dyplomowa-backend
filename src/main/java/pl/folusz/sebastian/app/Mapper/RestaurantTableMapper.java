package pl.folusz.sebastian.app.Mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.folusz.sebastian.app.DTO.RestaurantTableDto;
import pl.folusz.sebastian.app.entity.restaurantTable.RestaurantTable;

import java.util.List;

@Mapper
public interface RestaurantTableMapper {
    RestaurantTableMapper MAPPER = Mappers.getMapper(RestaurantTableMapper.class);

    RestaurantTableDto restaurantTableToRestaurantTableDto(RestaurantTable restaurantTable);
    List<RestaurantTableDto> restaurantTableToRestaurantTableDto(List<RestaurantTable> restaurantTable);
}
