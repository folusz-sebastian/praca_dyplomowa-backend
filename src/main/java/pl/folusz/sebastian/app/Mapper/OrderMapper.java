package pl.folusz.sebastian.app.Mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.folusz.sebastian.app.DTO.OrderDto;
import pl.folusz.sebastian.app.entity.order.Order;

import java.util.List;

@Mapper
public interface OrderMapper {
    OrderMapper MAPPER = Mappers.getMapper(OrderMapper.class);

    OrderDto orderToOrderDto (Order order);
    List<OrderDto> orderToOrderDto (List<Order> order);
}
