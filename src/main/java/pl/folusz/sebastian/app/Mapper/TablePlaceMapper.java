package pl.folusz.sebastian.app.Mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.folusz.sebastian.app.DTO.TablePlaceDto;
import pl.folusz.sebastian.app.entity.restaurantTable.TablePlace;

@Mapper
public interface TablePlaceMapper {
    TablePlaceMapper MAPPER = Mappers.getMapper(TablePlaceMapper.class);

    TablePlaceDto tablePlaceTotablePlaceDto(TablePlace tablePlace);
}
