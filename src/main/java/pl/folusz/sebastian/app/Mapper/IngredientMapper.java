package pl.folusz.sebastian.app.Mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.folusz.sebastian.app.DTO.IngredientDto;
import pl.folusz.sebastian.app.entity.dish.Ingredient;

import java.util.List;

@Mapper
public interface IngredientMapper {
    IngredientMapper MAPPER = Mappers.getMapper(IngredientMapper.class);

    IngredientDto ingredientToIngrdientDto(Ingredient ingredient);
    List<IngredientDto> ingredientToIngrdientDto(List<Ingredient> ingredient);
}
