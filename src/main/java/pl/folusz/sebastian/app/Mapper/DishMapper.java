package pl.folusz.sebastian.app.Mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.folusz.sebastian.app.DTO.DishDto;
import pl.folusz.sebastian.app.entity.dish.Dish;
import java.util.List;

@Mapper
public interface DishMapper {
    DishMapper MAPPER = Mappers.getMapper(DishMapper.class);

    DishDto dishToDishDto(Dish dish);
    List<DishDto> dishToDishDto(List<Dish> dish);
}
