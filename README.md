# Backend for restaurant management system mobile app

## Opis projektu
Repozytorium jest częścią [projektu](https://bitbucket.org/folusz-sebastian/workspace/projects/RMSMA) będącego aplikacją mobilną systememu zarządzania restauracją.

Aplikacja została napisana w języku Java przy wykorzystaniu framework-a Spring Boot.

Dodatkowo w aplikacji zostały wykorzystane takie moduły/biblioteki jak:

- spring security
- spring JPA
- spring REST
- mapStruct
- Lombok
